FROM ubuntu:bionic

RUN apt-get update && apt install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev curl wget unzip git -y && mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

CMD ["printenv"]