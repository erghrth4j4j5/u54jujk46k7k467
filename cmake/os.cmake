if (WIN32)
    set(gradlew_OS_WIN ON)
elseif (APPLE)
    set(gradlew_OS_APPLE ON)

    if (IOS OR CMAKE_SYSTEM_NAME STREQUAL iOS)
        set(gradlew_OS_IOS ON)
    else()
        set(gradlew_OS_MACOS ON)
    endif()
else()
    set(gradlew_OS_UNIX ON)

    if (ANDROID OR CMAKE_SYSTEM_NAME MATCHES "Android")
        set(gradlew_OS_ANDROID ON)
    elseif(CMAKE_SYSTEM_NAME MATCHES "Linux")
        set(gradlew_OS_LINUX ON)
    elseif(CMAKE_SYSTEM_NAME STREQUAL FreeBSD)
        set(gradlew_OS_FREEBSD ON)
    endif()
endif()


if (gradlew_OS_WIN)
    add_definitions(/DWIN32)
    add_definitions(/Dgradlew_OS_WIN)
elseif(gradlew_OS_APPLE)
    add_definitions(/Dgradlew_OS_APPLE)

    if (gradlew_OS_IOS)
        add_definitions(/Dgradlew_OS_IOS)
    else()
        add_definitions(/Dgradlew_OS_MACOS)
    endif()
elseif(gradlew_OS_UNIX)
    add_definitions(/Dgradlew_OS_UNIX)

    if (gradlew_OS_ANDROID)
        add_definitions(/Dgradlew_OS_ANDROID)
    elseif (gradlew_OS_LINUX)
        add_definitions(/Dgradlew_OS_LINUX)
    elseif (gradlew_OS_FREEBSD)
        add_definitions(/Dgradlew_OS_FREEBSD)
    endif()
endif()
