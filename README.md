# gradlew

[![Github All Releases](https://img.shields.io/github/downloads/gradlew/gradlew/total.svg)](https://github.com/gradlew/gradlew/releases)
[![GitHub release](https://img.shields.io/github/release/gradlew/gradlew/all.svg)](https://github.com/gradlew/gradlew/releases)
[![GitHub Release Date](https://img.shields.io/github/release-date/gradlew/gradlew.svg)](https://github.com/gradlew/gradlew/releases)
[![GitHub license](https://img.shields.io/github/license/gradlew/gradlew.svg)](https://github.com/gradlew/gradlew/blob/master/LICENSE)
[![GitHub stars](https://img.shields.io/github/stars/gradlew/gradlew.svg)](https://github.com/gradlew/gradlew/stargazers)
[![GitHub forks](https://img.shields.io/github/forks/gradlew/gradlew.svg)](https://github.com/gradlew/gradlew/network)

gradlew is a high performance, open source, cross platform RandomX, KawPow, CryptoNight and AstroBWT unified CPU/GPU miner. Official binaries are available for Windows, Linux, macOS and FreeBSD.

## Mining backends
- **CPU** (x64/ARMv8)
- **OpenCL** for AMD GPUs.
- **CUDA** for NVIDIA GPUs via external [CUDA plugin](https://github.com/gradlew/gradlew-cuda).

## Download
* **[Binary releases](https://github.com/gradlew/gradlew/releases)**
* **[Build from source](https://gradlew.com/docs/miner/build)**

## Usage
The preferred way to configure the miner is the [JSON config file](src/config.json) as it is more flexible and human friendly. The [command line interface](https://gradlew.com/docs/miner/command-line-options) does not cover all features, such as mining profiles for different algorithms. Important options can be changed during runtime without miner restart by editing the config file or executing API calls.

* **[Wizard](https://gradlew.com/wizard)** helps you create initial configuration for the miner.
* **[Workers](http://workers.gradlew.info)** helps manage your miners via HTTP API.

## Donations
* Default donation 1% (1 minute in 100 minutes) can be increased via option `donate-level` or disabled in source code.
* XMR: `48edfHu7V9Z84YzzMa6fUueoELZ9ZRXq9VetWzYGzKt52XU5xvqgzYnDK9URnRoJMk1j8nLwEVsaSWJ4fhdUyZijBGUicoD`

## Developers
* **[gradlew](https://github.com/gradlew)**
* **[sech1](https://github.com/SChernykh)**

## Contacts
* support@gradlew.com
* [reddit](https://www.reddit.com/user/gradlew/)
* [twitter](https://twitter.com/gradlew_dev)
